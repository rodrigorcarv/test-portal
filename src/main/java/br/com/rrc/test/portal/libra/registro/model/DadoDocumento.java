package br.com.rrc.test.portal.libra.registro.model;

public class DadoDocumento {
	
	private String viagem;
	private String armador;
	private String cliente;
	private String portoEmbarque;
	private String portoDestino;
	
	public DadoDocumento(String viagem, String armador, String cliente, String portoEmbarque, String portoDestino) {
		super();
		this.viagem = viagem;
		this.armador = armador;
		this.cliente = cliente;
		this.portoEmbarque = portoEmbarque;
		this.portoDestino = portoDestino;
	}
	
	
	public String getViagem() {
		return viagem;
	}
	public void setViagem(String viagem) {
		this.viagem = viagem;
	}
	public String getArmador() {
		return armador;
	}
	public void setArmador(String armador) {
		this.armador = armador;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getPortoEmbarque() {
		return portoEmbarque;
	}
	public void setPortoEmbarque(String portoEmbarque) {
		this.portoEmbarque = portoEmbarque;
	}
	public String getPortoDestino() {
		return portoDestino;
	}
	public void setPortoDestino(String portoDestino) {
		this.portoDestino = portoDestino;
	}
	
	

}
