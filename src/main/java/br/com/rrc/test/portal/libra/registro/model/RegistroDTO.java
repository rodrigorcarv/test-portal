package br.com.rrc.test.portal.libra.registro.model;

public class RegistroDTO {
	
	private String numeroDocumentoAverbacao;
	private String tipoDocumento;
	private String numeroDocumento;
	private String dataDoDocumento;
	
	public String getNumeroDocumentoAverbacao() {
		return numeroDocumentoAverbacao;
	}
	
	public void setNumeroDocumentoAverbacao(String numeroDocumentoAverbacao) {
		this.numeroDocumentoAverbacao = numeroDocumentoAverbacao;
	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	public String getDataDoDocumento() {
		return dataDoDocumento;
	}
	
	public void setDataDoDocumento(String dataDoDocumento) {
		this.dataDoDocumento = dataDoDocumento;
	}
}