 # language: pt
 	@LoginCucumberTeste
	Funcionalidade: Login
	   O sistema deve logar no sistema de forma correta

	Cenário: Como um usuário válido, posso logar no sistema
	    Dado que acesso a tela de Login "http://172.26.221.244/"
	    Quando digitar o usuario "paula.sousa@fcamara.com.br" e senha "libra12345"
	    E clicar no botão de login
	    Então acesso a tela incial do sistema como "Paula Sousa"
	    
	Cenário: Como um usuário inválido, posso logar no sistema
	    Dado que acesso a tela de Login "http://172.26.221.244/"
	    Quando digitar o usuario "paula.sousa@fcamara.com.br" e senha "libra"
	    E clicar no botão de login
	    Então o sistema deve apresentar a mensagem de "Falha na Autenticação. Por favor, tente novamente."