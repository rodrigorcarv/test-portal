 # language: pt
 	@RegistroCucumberTeste
	Funcionalidade: Busca Registro
	   O sistema deve busca o um registro de maneira correta
	  
	@LoginDefault
 	@LogoutDefault
	Cenário: Como um usuário válido, buscar registro válido.
	    Dado que acesso a tela de registro "http://172.26.221.244/group/ltr/registro"
	    Quando informar o tipo de documento "BL" e o número do documento "TKBL1527_BL"
	    E clicar no botão de pesquisar
	    Então apresenta os dados do documento "Documento"
	    | viagem     | armador | cliente | portoEmbarque | portoDestino |
        | TESTE_EXP  | CMA CGM |         | HKG           | RIO          |
        
    @LoginDefault
 	@LogoutDefault    
    Cenário: Como um usuário válido, buscar registro inválido.
	    Dado que acesso a tela de registro "http://172.26.221.244/group/ltr/registro"
	    Quando informar o tipo de documento "BL" e o número do documento "NAO_EXISTE"
	    E clicar no botão de pesquisar
	    Então apresenta mensagem de número do documento inválido "Falha na Autenticação. Por favor, tente novamente."
		