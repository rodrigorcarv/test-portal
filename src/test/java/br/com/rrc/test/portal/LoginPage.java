package br.com.rrc.test.portal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

	private WebDriver webDriver;
	private String baseUrl =  "http://172.26.221.244/";
	
	public LoginPage (WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public void visita() {
		webDriver.get(baseUrl + "/web/guest/tela-inicial?p_p_id=58&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&saveLastPath=false&_58_struts_action=%2Flogin%2Flogin");
	}
	
	public void visita(String url) {
		webDriver.get(url);
	}
	
	public void preencherFormulario(String usuario, String senha) {
		WebElement txtUsuario = webDriver.findElement(By.id("_58_login"));
		WebElement txtSenha = webDriver.findElement(By.id("_58_password"));
		
		txtUsuario.sendKeys(usuario);
		txtSenha.sendKeys(senha);
	}
	
	public void efetuarLogin(String botao) {
		webDriver.findElement(By.xpath("//input[@id='_58_login']")).submit(); 
	}
	
	public void efetuarLogin() {
		webDriver.findElement(By.xpath("//input[@id='_58_login']")).submit(); 
	}
	
	public void efetuarLogout() {
		
		webDriver.findElement(By.id("_145_userAvatar")).click();
		webDriver.findElement(By.className("icon-off")).click();
	}
	
	public boolean isRealizadoComSucesso(String nomeUsuario) {
		return webDriver.getPageSource().contains(nomeUsuario);
	}
	
	public boolean isFalhaLogin(String mensagem) {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		wait.until(
				ExpectedConditions.invisibilityOfElementWithText(By.className("nav-item-label"), "Entrar"));

		String mensagemFalha = webDriver.findElement(By.xpath("//form[@id='_58_fm'] //div[@class='alert alert-error']")).getText();
		return mensagemFalha.equals(mensagem);
	}
}
