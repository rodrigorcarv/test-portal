package br.com.rrc.test.portal.registro;

import org.openqa.selenium.WebDriver;

public class RegistroPage {
	
	private WebDriver webDriver;
	
	public RegistroPage (WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public void visita() {
		webDriver.get("http://172.26.221.244/group/ltr/registro");
	}
	
	public void visita(String url) {
		webDriver.get(url);
	}

	public NovoRegistroPage novo() {
		return new NovoRegistroPage(webDriver);
	}
	
	public BuscaRegistroPage buscaRegistroPage() {
		return new BuscaRegistroPage(webDriver);
	}
}
