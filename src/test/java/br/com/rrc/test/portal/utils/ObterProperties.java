package br.com.rrc.test.portal.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ObterProperties {
	
	public Properties lerArquivo(String nomeProperties) {
		
		Properties props = new Properties();
		try {
			FileInputStream file;
			file = new FileInputStream(String.format("%s%s%s", "src/test/resources/properties/", nomeProperties,".properties"));
			props.load(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Erro ao ler arquivo");
		} catch (IOException e) {
			throw new RuntimeException("Erro ao ler arquivo");
		}
		return props;
	}
}
