package br.com.rrc.test.portal.registro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.rrc.test.portal.libra.registro.model.DadoDocumento;

public class BuscaRegistroPage {
	
private WebDriver webDriver;
	
	public BuscaRegistroPage (WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public void buscarBLOrBooking(String tipoConsulta, String numeroDocumento) {
		
		Select selTipoConsulta = new Select(webDriver.findElement(By.id("blCustomDFFCE")));
		selTipoConsulta.selectByValue(tipoConsulta);
		
		WebElement numeroBl = webDriver.findElement(By.id("blNbr"));
		numeroBl.sendKeys(numeroDocumento);
	}
	
	public void buscarPorNumeroConteiner(String numeroConteiner) {
		
		Select selTipoConsulta = new Select(webDriver.findElement(By.id("blCustomDFFCE")));
		selTipoConsulta.selectByVisibleText("Conteiner");
		
		WebElement numeroBl = webDriver.findElement(By.id("blNbrContainer"));
		numeroBl.sendKeys(numeroConteiner);
	}
	
	public void efetuarPesquisa() {
		WebElement pesquisar = webDriver.findElement(By.className("icon-search"));
		pesquisar.click();
	}
	
	public boolean documentoNaoEncontrado(String mensagem) {
		 return webDriver.getPageSource().contains(mensagem);
	}
	
	public void confirmacaoDocumentoNaoEncontrado(){
		
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		
		WebElement data = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("showDialogAlertErro")));
		data.findElement(By.className("icon-ok")).click();
	}
	
	public boolean existeBLOrBooking(DadoDocumento dadoDocumento) {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		boolean isContainsViagem = wait.until(
				ExpectedConditions.attributeContains(By.id("blCarrierVisit"), "value", dadoDocumento.getViagem()));

		boolean isContainsArmador = webDriver.findElement(By.id("blLineOperator")).getAttribute("value").equals(dadoDocumento.getArmador());
		boolean isContainsCliente = webDriver.findElement(By.id("blConsignee")).getAttribute("value").equals(dadoDocumento.getCliente());
		boolean isContainsPortoDestino = webDriver.findElement(By.id("blPod1")).getAttribute("value").equals(dadoDocumento.getPortoDestino());
		boolean isContainsPortoEmbarque = webDriver.findElement(By.id("blPod2")).getAttribute("value").equals(dadoDocumento.getPortoEmbarque());
		
		return isContainsViagem &&
				isContainsArmador &&
				isContainsCliente && 
				isContainsPortoDestino &&
				isContainsPortoEmbarque;
	}
}


