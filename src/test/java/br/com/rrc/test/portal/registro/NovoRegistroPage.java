package br.com.rrc.test.portal.registro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.rrc.test.portal.libra.registro.model.RegistroDTO;

public class NovoRegistroPage {
	
	private WebDriver webDriver;
	
	public NovoRegistroPage (WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public void averbarRegistro(RegistroDTO registroDTO) {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("LstContainer")));
		
		Select tipoDocumento = new Select(webDriver.findElement(By.id("tipoDoc")));
		tipoDocumento.selectByValue(registroDTO.getTipoDocumento());
		
		WebElement numeroDocumento = webDriver.findElement(By.id("customdNbr"));
		numeroDocumento.sendKeys(registroDTO.getNumeroDocumento());
		
		webDriver.findElement(By.id("activeAba2")).click();
		webDriver.findElement(By.id("dtDocumento")).click();
		
		WebElement data = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.linkText("28")));
		data.click();
		
		WebElement salvar = webDriver.findElement(By.id("btnSalvarDoc"));
		salvar.click();
	}
	
	public void confimarAverbacaoRegistro() {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		
		WebElement data = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.className("modalOkButton")));
		data.findElement(By.className("icon-ok")).click();
	}
	
	public boolean registroSalvoComSucesso(String mensagemSucesso) {
		return webDriver.getPageSource().contains(mensagemSucesso);
	}
}
