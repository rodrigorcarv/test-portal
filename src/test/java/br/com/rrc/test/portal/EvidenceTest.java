package br.com.rrc.test.portal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.eliasnogueira.selenium.evidence.EvidenceReport;
import com.eliasnogueira.selenium.evidence.EvidenceType;
import com.eliasnogueira.selenium.evidence.SeleniumEvidence;
import com.eliasnogueira.selenium.evidence.report.GenerateEvidenceReport;


public class EvidenceTest {

	private WebDriver webDriver;
	
	List<SeleniumEvidence> evidenceList = null;
	EvidenceReport report = null;
	String errorMessage = null;
	
	@Before
	public void incializa() throws Exception {
		webDriver = new FirefoxDriver();
		evidenceList = new ArrayList<SeleniumEvidence>();
	}

	@After
	public void tearDown() throws Exception {
		webDriver.quit();
	}

	@Test
	public void generatEvidence() throws IOException {
		
		LoginPage loginPage = new LoginPage(webDriver);
		
		
		try {
			loginPage.visita();
			evidenceList.add(new SeleniumEvidence("visita", takeScreenshot(webDriver)));
			
			loginPage.preencherFormulario("paula.sousa@fcamara.com.br", "libra12345");
			evidenceList.add(new SeleniumEvidence("preencherFormulario", takeScreenshot(webDriver)));	
			
			loginPage.efetuarLogin();
			evidenceList.add(new SeleniumEvidence("efetuarLogin", takeScreenshot(webDriver)));

			loginPage.efetuarLogout();
			evidenceList.add(new SeleniumEvidence("efetuarLogin", takeScreenshot(webDriver)));
			
		} catch (Exception e) {
			
		} finally {
			EvidenceReport report = new EvidenceReport(evidenceList, "MyReportOK", "Elias", "ProjectTest", null);
			GenerateEvidenceReport.generareEvidenceReport(report, EvidenceType.DOC);
		}
	}
	
	private String takeScreenshot(WebDriver driver) {
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
	}
}
