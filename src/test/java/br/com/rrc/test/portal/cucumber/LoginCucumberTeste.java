package br.com.rrc.test.portal.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( format = { "pretty", "html:target/cucumber-html-report" }, features = "classpath:login", tags = "@LoginCucumberTeste", 
glue = "br.com.rrc.test.portal.cucumber.passos.Login", monochrome = true, dryRun = false )
public class LoginCucumberTeste {

}

