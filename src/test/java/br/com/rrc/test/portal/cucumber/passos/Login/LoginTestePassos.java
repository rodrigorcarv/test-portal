package br.com.rrc.test.portal.cucumber.passos.Login;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.rrc.test.portal.LoginPage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LoginTestePassos {
	
	private WebDriver webDriver;
	private LoginPage loginPage;
	
	@Dado("^que acesso a tela de Login \"(.*?)\"$")
	public void que_acesso_a_tela_de_login(String url) throws Throwable {
		webDriver = new FirefoxDriver();
		loginPage = new LoginPage(webDriver);
		loginPage.visita(url);
	}

	@Quando("^digitar o usuario \"(.*?)\" e senha \"(.*?)\"$")
	public void digito_o_usuario_e_senha(String usuario, String senha) throws Throwable {
		loginPage.preencherFormulario(usuario, senha);
	}

	@E("clicar no botão de login")
	public void clicar_no_botao_de_login() throws Throwable {
		loginPage.efetuarLogin();
	}
	
	@Entao("^acesso a tela incial do sistema como \"(.*?)\"$")
	public void acessa_tela_incial_do_sistema (String nomeUsuario) throws Throwable {
		Assert.assertTrue(loginPage.isRealizadoComSucesso(nomeUsuario));
		webDriver.close();
	}
	
	@Entao("^o sistema deve apresentar a mensagem de \"(.*?)\"$")
	public void o_sistema_deve_apresentar_a_mensagem_de (String mensagem) throws Throwable {
		Assert.assertTrue(loginPage.isFalhaLogin(mensagem));
		webDriver.close();
	}
}
