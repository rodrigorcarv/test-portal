package br.com.rrc.test.portal.registro.test.system;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.rrc.test.portal.LoginPage;
import br.com.rrc.test.portal.libra.registro.model.RegistroDTO;
import br.com.rrc.test.portal.registro.RegistroPage;
import cucumber.api.java.After;

public class NovoRegistroSystemTest {
	
	private WebDriver webDriver;
	private RegistroPage registroPage;
	private LoginPage loginPage;
	
	@Before
	public void incializa() throws Exception {
		webDriver = new FirefoxDriver();

		loginPage = new LoginPage(webDriver);
		loginPage.visita();
		loginPage.preencherFormulario("paula.sousa@fcamara.com.br", "libra12345");
		loginPage.efetuarLogin();

		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("navMenuControls")));

		registroPage = new RegistroPage(webDriver);
	}

	@After
	public void finaliza() {

		loginPage.efetuarLogout();
		webDriver.close();
		webDriver.quit();
	}

	@Test
	public void deveCadastraUmRegisro() throws Exception {

		registroPage.visita();
		registroPage.buscaRegistroPage().buscarBLOrBooking("BL", "REGI01042015RAC3");
		registroPage.novo().averbarRegistro(criarRegistro());
		registroPage.novo().registroSalvoComSucesso("Averbação inserida com sucesso.");
		registroPage.novo().confimarAverbacaoRegistro();
	}
	
	private RegistroDTO criarRegistro() {

		RegistroDTO registroDTO = new RegistroDTO();
		registroDTO.setNumeroDocumentoAverbacao("REGI01042015RAC3");
		registroDTO.setTipoDocumento("10712614");
		registroDTO.setNumeroDocumento("1071261410712614");
		registroDTO.setDataDoDocumento("26");
		
		return registroDTO;
		
	}

}
