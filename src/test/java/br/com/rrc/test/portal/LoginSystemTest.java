package br.com.rrc.test.portal;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class LoginSystemTest {

	private WebDriver webDriver;

	@Before
	public void incializa() throws Exception {
		webDriver = new FirefoxDriver();
	}

	@Test
	public void entraNoPortal() throws Exception {
		
		LoginPage loginPage = new LoginPage(webDriver);
		
		loginPage.visita();
		loginPage.preencherFormulario("paula.sousa@fcamara.com.br", "libra12345");
		loginPage.efetuarLogin();
		loginPage.efetuarLogout();
		
		webDriver.close();
	}
}
