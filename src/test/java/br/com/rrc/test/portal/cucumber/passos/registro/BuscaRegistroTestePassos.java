package br.com.rrc.test.portal.cucumber.passos.registro;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.rrc.test.portal.LoginPage;
import br.com.rrc.test.portal.libra.login.LoginDTO;
import br.com.rrc.test.portal.libra.registro.model.DadoDocumento;
import br.com.rrc.test.portal.registro.BuscaRegistroPage;
import br.com.rrc.test.portal.registro.RegistroPage;
import br.com.rrc.test.portal.utils.ObterDadosLogin;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class BuscaRegistroTestePassos {

	private WebDriver webDriver;
	private RegistroPage registroPage;
	private BuscaRegistroPage buscaRegistroPage;
	private LoginPage loginPage;
	
	@After("@LogOutDefault")
	public void logout () {
		loginPage.efetuarLogout();
	}
	
	@Before("@LoginDefault")
	public void login () {
		
		ObterDadosLogin obterDadosLogin = new ObterDadosLogin();
		LoginDTO login = obterDadosLogin.getLogin();
		
		webDriver = new FirefoxDriver();
		loginPage = new LoginPage(webDriver);
		loginPage.visita(login.getUrl());
		loginPage.preencherFormulario(login.getUsuario(), login.getSenha());
		loginPage.efetuarLogin();
	}
	
	@Dado("^que acesso a tela de registro \"(.*?)\"$")
	public void que_acesso_a_tela_de_registro(String url) throws Throwable {
		registroPage = new RegistroPage(webDriver);
		registroPage.visita(url);
	}

	@Quando("^informar o tipo de documento \"(.*?)\" e o número do documento \"(.*?)\"$")
	public void informar_o_tipo_de_documento_e_numero_de_documento (String tipoDocumento, String numeroDocumento) throws Throwable {
		buscaRegistroPage = registroPage.buscaRegistroPage();
		buscaRegistroPage.buscarBLOrBooking(tipoDocumento, numeroDocumento);
	}

	@E("clicar no botão de pesquisar")
	public void clicar_no_botao_de_pesquisar() throws Throwable {
		buscaRegistroPage.efetuarPesquisa();
	}

	@Entao("^apresenta os dados do documento \"(.*?)\"$")
	public void apresenta_os_dados_do_documento (String documento, List<DadoDocumento> dadoDocumentos) throws Throwable {
		Assert.assertTrue(buscaRegistroPage.existeBLOrBooking(dadoDocumentos.get(0)));
		webDriver.close();
	}

	@Entao("^apresenta mensagem de número do documento inválido \"(.*?)\"$")
	public void apresenta_mensagem_de_numero_do_documento_invalido (String mensagem) throws Throwable {
		buscaRegistroPage.documentoNaoEncontrado(mensagem);
		buscaRegistroPage.confirmacaoDocumentoNaoEncontrado();
		webDriver.close();
	}
}
