package br.com.rrc.test.portal.utils;

import java.util.Properties;

import br.com.rrc.test.portal.libra.login.LoginDTO;

public class ObterDadosLogin {
	
	public LoginDTO getLogin() {
		
		ObterProperties obterProperties = new ObterProperties();
		Properties prop = obterProperties.lerArquivo("login");
		
		LoginDTO loginDTO = new LoginDTO();
		loginDTO.setUrl(prop.getProperty("login.url"));
		loginDTO.setUsuario(prop.getProperty("login.usuario"));
		loginDTO.setSenha(prop.getProperty("login.senha"));
		
		return loginDTO;
	}
}
